﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI
{
    class ResultGridRow
    {
        public string Name { get; }
        public string Result { get; }
        public bool isBanned { get; }
        private double resultValue;


        public ResultGridRow(string name)
        {
            Name = name;
            Result = string.Format("{0:P2}", 0);
            resultValue = 0;
            isBanned = false;
        }

        public ResultGridRow(string name, double result)
        {
            Name = name;
            Result = string.Format("{0:P2}", result);
            resultValue = result;
            isBanned = false;
        }

        public double GetResultValue()
        {
            return resultValue;
        }
    }
}
