﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Logic;

namespace GUI
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string FILE_PATH = "State.txt";

        public List<IName> names { get; }
        public State currentState { get; }

        public MainWindow()
        {
            InitializeComponent();

            
            //Load State
            if (File.Exists(FILE_PATH))
            {
                try
                {
                    currentState = State.Deserialize(FILE_PATH);
                }
                catch (IOException)
                {
                    currentState = new State(new List<IUser>(), new List<IFamilyAccount>(), null, null);
                }
            }
            else
                currentState = new State(new List<IUser>(), new List<IFamilyAccount>(), null, null);

            //Load Names
            INameGenderLoader nameLoader = new NameDatabaseLoader();
            List<IName> names = new List<IName>();
            IEnumerable<IName> temporaryNames = null;
            nameLoader.TryLoadGenderNames(out temporaryNames, Gender.Male);
            names.AddRange(temporaryNames);
            nameLoader.TryLoadGenderNames(out temporaryNames, Gender.Female);
            names.AddRange(temporaryNames);

            //FAKE DATA CREATION
            User userNew = new User("newUser", new List<IFamilyTests>());
            currentState.users.Add(userNew);
            IFamilyAccount family = new FamilyAccount("Kot", names, new List<IUser>(), BabyGender.Male);
            currentState.familyAccounts.Add(family);
            currentState.activeFamily = family;
            currentState.activeUser = userNew;
            
            UpdateNotFinishedComboBox();
            UpdateFamiliesUsers();
        }

        private void UpdateFamiliesUsers()
        {
            ActiveFamilyComboBox.ItemsSource = currentState.familyAccounts.Select(p => p.FamilyId);
            ActiveUserComboBox.ItemsSource = currentState.users.Select(p => p.userName);
        }

        private void UpdateNotFinishedComboBox()
        {
            IFamilyTests familyTests = currentState.activeUser?.FamiliesTests.FirstOrDefault(p => p.familyAccount == currentState.activeFamily);
            if (familyTests == null)
                return;
            IEnumerable<string> comboBoxValues = familyTests.tests.Where(p => p.ifFinished == false)
                .Select(p => p.namePair.Item1.name + " " + p.namePair.Item2.name);
            NotFinishedTestsComboBox.ItemsSource = comboBoxValues;
            NotFinishedTestsComboBox.SelectedItem = comboBoxValues.Last();
        }

        private void RefreshLeftRightButtons(ITest test)
        {
            if (test == null)
            {
                leftButton.Visibility = Visibility.Hidden;
                rightButton.Visibility = Visibility.Hidden;
            }
            else
            {
                leftButton.Content = test.namePair.Item1.name;
                rightButton.Content = test.namePair.Item2.name;
                leftButton.Visibility = Visibility.Visible;
                rightButton.Visibility = Visibility.Visible;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ITest test = currentState.activeUser.GenerateTest(currentState.activeFamily);
            RefreshLeftRightButtons(test);
            UpdateNotFinishedComboBox();
        }
        
        private void leftButton_Click(object sender, RoutedEventArgs e)
        {
            currentState.activeUser.FamiliesTests.First(p => p.familyAccount == currentState.activeFamily).ActiveTest.FinishTest(TestWinner.LeftWon);
            UpdateGUIAfterTestFinish(sender, e);
        }

        private void rightButton_Click(object sender, RoutedEventArgs e)
        {
            currentState.activeUser.FamiliesTests.First(p => p.familyAccount == currentState.activeFamily).ActiveTest.FinishTest(TestWinner.RightWon);
            UpdateGUIAfterTestFinish(sender,e);
        }

        private void BannedCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            IName name = currentState.activeFamily.names.First(p => p.name == (string)(((CheckBox)sender).Tag));
            currentState.activeFamily.names.Remove(name);
        }

        private void UpdateGUIAfterTestFinish(object sender, RoutedEventArgs e)
        {
            resultGrid.ItemsSource = currentState.activeUser.GetResultsForFamily(currentState.activeFamily).ResultsDictionary
                .Select(p => new ResultGridRow(p.Key.name, p.Value)).OrderByDescending(p => p.GetResultValue());
            resultGrid.Items.Refresh();
            Button_Click(sender, e);
        }

        private void NotFinishedTestsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (NotFinishedTestsComboBox.SelectedItem == null)
                return;
            string selectedValue = NotFinishedTestsComboBox.SelectedItem.ToString();
            string left = selectedValue.Substring(0, selectedValue.IndexOf(" "));
            string right = selectedValue.Remove(0, selectedValue.IndexOf(" ") + 1);
            IFamilyTests activeFamilyTests = currentState.activeUser.FamiliesTests.First(p => p.familyAccount == currentState.activeFamily);
            ITest test = activeFamilyTests.tests.Where(p => p.ifFinished == false).First(p => p.namePair.Item1.name == left && p.namePair.Item2.name == right);
            activeFamilyTests.ActiveTest = test;
            RefreshLeftRightButtons(test);
        }

        private void TabItem_Selected_1(object sender, RoutedEventArgs e)
        {
            BannedDataGrid.ItemsSource = currentState.activeFamily.names.Select(p => new ResultGridRow(p.name));
        }

        private void ActiveUserComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string user = ActiveUserComboBox.SelectedItem.ToString();
            IUser userObject = currentState.users.First(p => p.userName == user);
            currentState.activeUser = userObject;
            ITest activeTest = currentState.activeUser.FamiliesTests.FirstOrDefault(p => p.familyAccount == currentState.activeFamily)?.ActiveTest;
            RefreshLeftRightButtons(activeTest);
            UpdateNotFinishedComboBox();
        }

        private void ActiveFamilyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string family = ActiveFamilyComboBox.SelectedItem.ToString();
            IFamilyAccount familyObject = currentState.familyAccounts.First(p => p.FamilyId == family);
            currentState.activeFamily = familyObject;
            ITest activeTest = currentState.activeUser.FamiliesTests.FirstOrDefault(p => p.familyAccount == currentState.activeFamily)?.ActiveTest;
            RefreshLeftRightButtons(activeTest);
            UpdateNotFinishedComboBox();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            BabyGender? choosenGender = null;
            if ((bool)NewFamilyGenderMaleRadioButton.IsChecked)
                choosenGender = BabyGender.Male;
            else if ((bool)NewFamilyGenderFemaleRadioButton.IsChecked)
                choosenGender = BabyGender.Female;
            else if((bool)NewFamilyGenderNotSpecifiedRadioButton.IsChecked)
                choosenGender = BabyGender.NotSpecified;
            currentState.familyAccounts.Add(new FamilyAccount(NewFamilyTextBox.Text,
                names,
                new List<IUser>(),
                (BabyGender)choosenGender
                ));
            UpdateFamiliesUsers();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            currentState.users.Add(new User(NewUserTextBox.Text, new List<IFamilyTests>()));
            UpdateFamiliesUsers();
        }
    }
}
