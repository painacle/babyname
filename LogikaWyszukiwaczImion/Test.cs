﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    [Serializable]
    class Test : ITest
    {
        public IUser user;
        public bool ifFinished { get; set; }
        public TestWinner testWinner;
        public DateTime testDateTime { get; private set; }
        public Tuple<IName, IName> namePair { get; set; }

        public Test(IUser user)
        {
            this.user = user;
            ifFinished = false;
            testWinner = TestWinner.TestNotFinished;
        }

        public void FinishTest (TestWinner testWinner)
        {           
            this.testWinner = testWinner;

            if (testWinner != TestWinner.TestNotFinished)
            {
                testDateTime = DateTime.Now;
                ifFinished = true;
            }
        }
        public IName GetWinnerName()
        {
            if(testWinner == TestWinner.LeftWon)
                return namePair.Item1;
            else if (testWinner == TestWinner.RightWon)
                return namePair.Item2;
            else
                return null;
        }
        public IName GetLoserName()
        {
            if (testWinner == TestWinner.LeftWon)
                return namePair.Item2;
            else if (testWinner == TestWinner.RightWon)
                return namePair.Item1;
            else
                return null;
        }
    
    }
}
