﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public interface IUser
    {
        string userName { get; set; }
        Results GetResultsForFamily(IFamilyAccount account);
        ICollection<IFamilyTests> FamiliesTests { get; }
        ITest GenerateTest(IFamilyAccount account);//funkcja która generuje pojedyńczy test dla użytkownika w odniesieniu do konkretnej rodziny
    }
}
