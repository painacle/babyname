namespace Logic.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Logic.BabyNameModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Logic.BabyNameModel context)
        {
            NameWithGender person = new NameWithGender("Jarmir Bobek", Gender.Female);
            NameWithGender person2 = new NameWithGender("Matyldusz", Gender.Female);
            context.names.Add(person);
            context.names.Add(person2);
        }
    }
}
