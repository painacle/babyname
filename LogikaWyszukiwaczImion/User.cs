﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    [Serializable]
    public class User : IUser
    {
        public string userName { get; set; }
        
        public ICollection<IFamilyTests> FamiliesTests { get; }

        public User(string userName, ICollection<IFamilyTests> familiesTests)
        {
            this.userName = userName;
            this.FamiliesTests = familiesTests;
        }

        public ITest GenerateTest(IFamilyAccount account)//funkcja która generuje pojedyńczy test dla użytkownika w odniesieniu do konkretnej rodziny
        {
            if (!account.users.Contains(this))
                account.users.Add(this);

            ITest test = new Test(this);//używa ttego usera
            test.namePair = NamePairGenerator.DrawPair(account.GetNamesWithGender());
            IFamilyTests familyTests = FamiliesTests.FirstOrDefault(p => p.familyAccount.Equals(account));
            if (familyTests == null)
            {
                familyTests = new FamilyTests(account, new List<ITest>());
                FamiliesTests.Add(familyTests);
            }
            familyTests.tests.Add(test);
            familyTests.ActiveTest = test;
            return test; //zwracasz zmienną, a nie typ, czyli np INTA...
        }

        public Results GetResultsForFamily(IFamilyAccount account)
        {
           Results results = new Results(new Dictionary<IName, double>());
            foreach (var familyTest in FamiliesTests)
            {
                if (familyTest.familyAccount == account)
                    foreach (var test in familyTest.tests)
                        if (test.ifFinished)
                        {
                            if (results.ResultsDictionary.ContainsKey(test.GetWinnerName()))
                                results.ResultsDictionary[test.GetWinnerName()] += 1;
                            else results.ResultsDictionary.Add(test.GetWinnerName(), 1);
                            if (results.ResultsDictionary.ContainsKey(test.GetLoserName()))
                                results.ResultsDictionary[test.GetLoserName()] -= 1;
                            else results.ResultsDictionary.Add(test.GetLoserName(), -1);
                        }
            }
            return results;
        }
    }
}
