﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public interface INameGenderLoader
    {

        IEnumerable<IName> LoadPopularNames(Gender gender);
        bool TryLoadGenderNames(out IEnumerable<IName> nameList, Gender gender);
    }
}
