﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public enum BabyGender
    {
        Female,
        Male,
        NotSpecified
    }

    public interface IFamilyAccount
    {
        string FamilyId { get; }
        ICollection<IUser> users { get; }
        ICollection<IName> names { get; }
        BabyGender babyGenderProperty { get; }
        IEnumerable<IName> GetNamesWithGender();
        Results GetResults();
    }
}
