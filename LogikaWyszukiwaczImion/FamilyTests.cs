﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    [Serializable]
    class FamilyTests : IFamilyTests
    {
        public IFamilyAccount familyAccount { get; set; }
        public ICollection<ITest> tests { get; set; }
        public ITest ActiveTest { get; set; }

        public FamilyTests(IFamilyAccount familyAccount, ICollection<ITest> tests)
        {
            this.familyAccount = familyAccount;
            this.tests = tests;
        }

    }
}
