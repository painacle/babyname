﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class NameGenderFileLoader : INameLoader
    {
        private readonly string filePath;

        public NameGenderFileLoader(string filePath)
        {
            this.filePath = filePath;
        }

        public bool TryLoadNames(out IEnumerable<IName> nameList)
        {
            nameList = null;
            List<IName> nameListConverted = nameList.ToList();

            try
            {
                using (StreamReader streamReader = new StreamReader(filePath))
                {
                    while (!streamReader.EndOfStream)
                    {
                        string line = streamReader.ReadLine();
                        int semicolon = line.IndexOf(';');
                        string gender = line.Remove(0, semicolon + 1);
                        if (gender == "FEMALE")
                            nameListConverted.Add(new NameWithGender(line.Substring(0, semicolon), Gender.Female));
                        else if (gender == "MALE")
                            nameListConverted.Add(new NameWithGender(line.Substring(0, semicolon), Gender.Male));
                    }
                }
                nameList = nameListConverted;
                return true;
            }
            catch (Exception)
            {
                nameList = null;
                return false;
            }
        }
    }
}
