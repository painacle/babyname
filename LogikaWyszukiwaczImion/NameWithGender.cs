﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public enum Gender
    {
        Male,
        Female
    }

    [Serializable]
    public class NameWithGender : Name
    {
        public Gender gender;

        public NameWithGender() : base()
        {
            //Needed for EF
        }

        public NameWithGender(string name, Gender gender) : base(name)
        {
            this.gender = gender;
        }
        public override void PrzedstawSie()
        {
            if(gender==Gender.Female)
                Console.WriteLine("Przedstawiam imię dziewczynki: " + name);
            else
                Console.WriteLine("Przedstawiam imię chłopca: " + name);
        }
    }
}
