﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public interface IFamilyTests
    {
        IFamilyAccount familyAccount { get; set; }
        ICollection<ITest> tests { get; set; }
        ITest ActiveTest { get; set; }
    }
}
