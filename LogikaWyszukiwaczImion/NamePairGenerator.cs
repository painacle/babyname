﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    class NamePairGenerator
    {
        static public Tuple<IName, IName> DrawPair(IEnumerable<IName> names)
        {
            if (names.Count() <= 1)
                return null;

            Random rnd = new Random();
            int a = rnd.Next(names.Count());
            int b = rnd.Next(names.Count() - 1);
            if (b >= a)
                b++;

            Tuple <IName, IName> para = new Tuple<IName, IName>(names.ElementAt(a), names.ElementAt(b));
            return para;
        }
    }
}
