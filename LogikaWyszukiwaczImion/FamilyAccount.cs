﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    [Serializable]
    public class FamilyAccount: IFamilyAccount //Klasa odpowiedzialna za trzymanie danych o wszystkich kontach które biorą udział w wyborze konkretnego imienia
    {
        Dictionary<Name, double> nameResults;//lista imion branych pod uwagę dla dziecka, rezultat (jako drugi parametr)

        public string FamilyId { get; }
        public ICollection<IUser> users { get; }
        public ICollection<IName> names { get; }
        public BabyGender babyGenderProperty { get; }
        
        public FamilyAccount(string familyId, ICollection<IName> names, ICollection<IUser> users, BabyGender babyGenderProperty = BabyGender.NotSpecified)
        {
            FamilyId = familyId;
            this.names = names;
            this.users = users;
            this.babyGenderProperty = babyGenderProperty;
        }

        public IEnumerable<IName> GetNamesWithGender()
        {
            IEnumerable<NameWithGender> namesWithGender =
                from nameWithGender in names
                where nameWithGender is NameWithGender
                where ((NameWithGender)nameWithGender).gender.Converter() == babyGenderProperty
                select (NameWithGender)nameWithGender;

            return namesWithGender;
        }

        public Results GetResults()
        {
            return users.Select(p => p.GetResultsForFamily(this)).Aggregate((p, r) => p + r);
        }
    }
}
