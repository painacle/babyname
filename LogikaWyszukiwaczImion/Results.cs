﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic
{
    public class Results : IResults
    {
        public IDictionary<IName, double> ResultsDictionary { get; private set; }

        public Results(IDictionary<IName, double> results)
        {
            ResultsDictionary = results;
        }

        public IResults Normalize()
        {
            //Normalize Values
            double maxResultValue = ResultsDictionary.Max(p => p.Value);
            maxResultValue = Math.Max(maxResultValue, 1);
            double minResultValue = ResultsDictionary.Min(p => p.Value);
            foreach (var testResultKey in ResultsDictionary.Keys.ToList())
                ResultsDictionary[testResultKey] = (ResultsDictionary[testResultKey] - minResultValue) / (maxResultValue - minResultValue);
            return this;
        }

        private IEnumerable<string> ReturnAsStrings()
        {
            return ResultsDictionary.OrderByDescending(p => p.Value)
                .Select(p => p.Key.name + " " + string.Format("{0:P2}", p.Value));
        }

        public string ReturnAsOneString()
        {
            return string.Join("\r\n", ReturnAsStrings());
        }

        public static Results operator + (Results left, Results right)
        {
            Dictionary<IName, double> resultsDictionary = new Dictionary<IName, double>();
            foreach (var element in left.ResultsDictionary)
                resultsDictionary.AddValueOrCreate(element);
            foreach (var element in right.ResultsDictionary)
                resultsDictionary.AddValueOrCreate(element);
            return new Results(resultsDictionary);
        }
    }
}