namespace Logic
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class BabyNameModel : DbContext
    {
        public BabyNameModel()
            : base("name=BabyNameModel")
        {
            //Commented out for now - can be used when we do not have an access to DB
            //Database.SetInitializer(new BabyNameModelSeedData());

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<BabyNameModel, Migrations.Configuration>());
        }

        public virtual DbSet<NameWithGender> names { get; set; }
    }
}