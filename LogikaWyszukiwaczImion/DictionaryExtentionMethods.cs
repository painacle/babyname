﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public static class DictionaryExtentionMethods
    {
        public static bool AddValueOrCreate<T>(this Dictionary<T, double> dictionary, KeyValuePair<T, double> element)
        {
            if (element.Equals(null) || dictionary.Equals(null) || element.Key.Equals(null) || element.Value.Equals(null))
                return false;
            if (dictionary.ContainsKey(element.Key))
                dictionary[element.Key] += element.Value;
            else
                dictionary[element.Key] = element.Value;
            return true;
        }
    }
}
