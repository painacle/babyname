﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public static class EnumConverter
    {
        public static BabyGender Converter(this Gender gender)
        {
            switch (gender)
            {
                case Gender.Female: return BabyGender.Female;
                case Gender.Male: return BabyGender.Male;
                default: throw new ArgumentOutOfRangeException("No sex");
            }
        }
    }
}
