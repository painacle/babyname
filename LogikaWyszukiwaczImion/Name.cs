﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    [Serializable]
    public class Name : IName
    {
        public int Id { get; set; }
        
        public string name { get; set; }

        public Name()
        {
            //Needed for EF
        }

        public Name(string name)
        {
            this.name = name;
        }
        public virtual void PrzedstawSie()
        {
            Console.WriteLine("Przedstawiam imię dziecka " + name);
        }
    }
}
