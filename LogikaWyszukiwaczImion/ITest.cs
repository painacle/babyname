﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public enum TestWinner
    {
        LeftWon,
        RightWon,
        TestNotFinished
    }

    public interface ITest
    {
        bool ifFinished { get; set; }

        IName GetWinnerName();
        IName GetLoserName();

        Tuple<IName, IName> namePair { get; set; }

        void FinishTest(TestWinner testWinner);
    }
}
