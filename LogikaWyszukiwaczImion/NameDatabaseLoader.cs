﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class NameDatabaseLoader : INameGenderLoader
    {
        public IEnumerable<IName> LoadPopularNames(Gender gender)
        {
            throw new NotImplementedException();
        }

        public bool TryLoadGenderNames(out IEnumerable<IName> nameList, Gender gender)
        {
            try
            {
                using (BabyNameModel babyNameModel = new BabyNameModel())
                {
                    nameList = babyNameModel.names.ToList();
                    return true;
                }
            }
            catch(Exception)
            {
                nameList = null;
                return false;
            }
        }
        
    }
}
