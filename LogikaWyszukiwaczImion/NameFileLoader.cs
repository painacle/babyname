﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class NameFileLoader:INameLoader
    {
        private readonly string filePath;

        public NameFileLoader(string filePath)
        {
            this.filePath = filePath;
        }

        public bool TryLoadNames(out IEnumerable<IName> nameList)
        {
            nameList = null;
            List<IName> nameListConverted = nameList.ToList();

            try
            {
                using (StreamReader streamReader = new StreamReader(filePath))
                {
                    while (!streamReader.EndOfStream)
                    {
                        string line = streamReader.ReadLine();
                        nameListConverted.Add(new Name(line));
                    }
                }
                nameList = nameListConverted;
                return true;
            }
            catch (Exception)
            {
                nameList = null;
                return false;
            }
        }
    }
}
