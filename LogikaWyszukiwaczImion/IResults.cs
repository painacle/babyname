﻿using System.Collections.Generic;

namespace Logic
{
    public interface IResults
    {
        IDictionary<IName, double> ResultsDictionary { get; }

        IResults Normalize();

        string ReturnAsOneString();
    }
}