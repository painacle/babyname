﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Logic
{
    public class NameWebLoader : INameLoader, INameGenderLoader
    {
        private string pattern;

        static private List<IName> LoaderSourceList(string namesSource, BabyGender babyGender)
        {
            string patternStatic = @"<li><a href=""\/w\/index\.php\?title=([\w%]+)&amp;action";

            Regex regex1 = new Regex(patternStatic);
            MatchCollection match1 = regex1.Matches(namesSource);

            patternStatic = @"<li><a href=""\/wiki\/([\w%]+)";

            Regex regex2 = new Regex(patternStatic);
            MatchCollection match2 = regex2.Matches(namesSource);

            var list1 = match1.Cast<Match>();
            var list2 = match2.Cast<Match>();
            var listMarge = list1.Concat(list2).ToList();

            List<IName> namesList = new List<IName>();

            foreach (var name in listMarge)
            {
                string decodedName = HttpUtility.UrlDecode(name.Groups[1].Value);

                if (babyGender==BabyGender.NotSpecified)
                    namesList.Add(new Name(decodedName));
                else if(babyGender==BabyGender.Male)
                    namesList.Add(new NameWithGender(decodedName, Gender.Male));
                else if (babyGender == BabyGender.Female)
                    namesList.Add(new NameWithGender(decodedName, Gender.Female));

            }
            return namesList;
        }

        public bool TryLoadGenderNames(out IEnumerable<IName> nameList, Gender gender)
        {
            nameList = new List<IName>();
            List<IName> nameList2 = nameList.ToList();

            string pageAdress = "https://pl.wiktionary.org/wiki/Indeks:Polski_-_Imiona";

            try
            {
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage httpResponseMessage = httpClient.GetAsync(pageAdress).Result;
                var httpResponseMessageContent = httpResponseMessage.Content;
                string httpResponseMessageString = httpResponseMessageContent.ReadAsStringAsync().Result;

                if (gender == Gender.Female)
                {
                    pattern = @"Abigail[\s\S]+Żywia";
                }

                else
                    pattern = @"Abdon[\s\S]+Żytek";

                Regex regex1 = new Regex(pattern);
                Match match = regex1.Match(httpResponseMessageString);
                string namesSource = match.Value;

                nameList = LoaderSourceList(namesSource, gender.Converter());

                return true;
            }
            catch (Exception)
            {
                nameList = null;
                return false;
            }
        }
        public bool TryLoadNames(out IEnumerable<IName> nameList)
        {
            nameList = null;
            List<IName> nameListConverted = nameList.ToList();

            string pageAdress = "https://pl.wiktionary.org/wiki/Indeks:Polski_-_Imiona";
            pattern = @"Abigail[\s\S]+Żytek";

            try
            {
                HttpClient httpClient = new HttpClient();
                HttpResponseMessage httpResponseMessage = httpClient.GetAsync(pageAdress).Result;
                var httpResponseMessageContent = httpResponseMessage.Content;
                string httpResponseMessageString = httpResponseMessageContent.ReadAsStringAsync().Result;

                Regex regex1 = new Regex(pattern);
                Match match = regex1.Match(httpResponseMessageString);
                string namesSource = match.Value;

                List<IName> namesList = LoaderSourceList(namesSource, BabyGender.NotSpecified);

                nameList = LoaderSourceList(namesSource, BabyGender.NotSpecified);

                return true;
            }
            catch (Exception)
            {
                nameList = null;
                return false;
            }
        }

        public IEnumerable<IName> LoadPopularNames(Gender gender)
        {
            string pageAdress = "https://pl.wiktionary.org/wiki/Indeks:Polski_-_Imiona";

            HttpClient httpClient = new HttpClient();
            HttpResponseMessage httpResponseMessage = httpClient.GetAsync(pageAdress).Result;

            if (httpResponseMessage.IsSuccessStatusCode)
            {
                var httpResponseMessageContent = httpResponseMessage.Content;
                string httpResponseMessageString = httpResponseMessageContent.ReadAsStringAsync().Result;
                pattern = @"Imiona męskie\n[\S\s]+<\/p>";

                Regex regex1 = new Regex(pattern);
                Match match = regex1.Match(httpResponseMessageString);
                string namesSource = match.Value;

                string patternStatic = @"\/([\w%]+)""";
                Regex regex2 = new Regex(patternStatic);
                MatchCollection match1 = regex2.Matches(namesSource);

                var list = match1.Cast<Match>();

                List<IName> namesList = new List<IName>();

                foreach (var name in list)
                {
                    string decodedName = HttpUtility.UrlDecode(name.Groups[1].Value);

                    if (decodedName.Last() == 'a' && gender == Gender.Female)
                        namesList.Add(new NameWithGender(decodedName, Gender.Female));
                    else if (decodedName.Last() != 'a' && gender == Gender.Male)
                        namesList.Add(new NameWithGender(decodedName, Gender.Male));
                }

                return namesList;
            }
            else
                return null;
        }
    }
}
