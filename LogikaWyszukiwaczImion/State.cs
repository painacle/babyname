﻿using Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    [Serializable]

    public class State
    {
        public ICollection<IUser> users { get; set; }
        public ICollection<IFamilyAccount> familyAccounts { get; set; }
        public IFamilyAccount activeFamily { get; set; }
        public IUser activeUser { get; set; }


        public State(ICollection<IUser> users, ICollection<IFamilyAccount> familyAccounts, IFamilyAccount activeFamily, IUser activeUser)
        {
            this.users = users;
            this.familyAccounts = familyAccounts;
            this.activeUser = activeUser;
            this.activeFamily = activeFamily;
        }

        public static State Deserialize(string filePath)
        {
            IFormatter formatter = new BinaryFormatter();
            State state = null;
            using (Stream deserializeStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                 state = (State)formatter.Deserialize(deserializeStream);
            }
            return state;
        }

        public void Serialize(string filePath)
        {
            IFormatter formatter = new BinaryFormatter();
            using (Stream serializeStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                formatter.Serialize(serializeStream, this);
            }
        }
    }
}
