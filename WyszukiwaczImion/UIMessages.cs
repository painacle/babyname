﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WyszukiwaczImion
{
    static class UIMessages
    {
        public static class ConsoleTitle
        {
            public static string GetConsoleTitle()
            {
                return ApplicationName + " [Family: " +
                    ActiveFamily + ", User: " + ActiveUser + "]";
            }

            private static string applicationName = "BabyName";
            public static string ApplicationName
            {
                private get
                {
                    if (applicationName == null)
                        return "Unknown App";
                    return applicationName;
                }
                set
                {
                    applicationName = value;
                }
            }
            private static string activeFamily;
            public static string ActiveFamily
            {
                private get
                {
                    if (activeFamily == null)
                        return "Unknown";
                    return activeFamily;
                }
                set
                {
                    activeFamily = value;
                }
            }
            private static string activeUser;
            public static string ActiveUser
            {
                private get
                {
                    if (activeUser == null)
                        return "Unknown";
                    return activeUser;
                }
                set
                {
                    activeUser = value;
                }
            }
        }

        private static void ClearConsoleAndPrintTitle()
        {
            Console.Clear();
            ConsoleWriteLineWithColor(ConsoleTitle.GetConsoleTitle(), ConsoleColor.Yellow);
        }

        private static void ConsoleWriteLineWithColor(string text, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private static void ConsoleWriteWithColor(string text, ConsoleColor color = ConsoleColor.Gray)
        {
            Console.ForegroundColor = color;
            Console.Write(text);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        private static void ConsoleWriteLineWithColor(KeyValuePair<string, ConsoleColor>[] wordsAndColors)
        {
            foreach (var wordAndColor in wordsAndColors)
            {
                ConsoleWriteWithColor(wordAndColor.Key, wordAndColor.Value);
            }
            Console.WriteLine();
        }

        public static void ConsoleWriteLineWithColorAndClear(string text, ConsoleColor color = ConsoleColor.Gray)
        {
            ClearConsoleAndPrintTitle();
            ConsoleWriteLineWithColor(text, color);
        }

        public static void ConsoleWriteLineWithColorAndClear(KeyValuePair<string, ConsoleColor>[] wordsAndColors)
        {
            ClearConsoleAndPrintTitle();
            foreach (var wordAndColor in wordsAndColors)
            {
                ConsoleWriteWithColor(wordAndColor.Key, wordAndColor.Value);
            }
            Console.WriteLine();
        }

        public static void InsertMessage()
        {
            ConsoleWriteLineWithColorAndClear(new KeyValuePair<string, ConsoleColor>[]
            {
                    new KeyValuePair<string, ConsoleColor>("Insert \"",ConsoleColor.Gray),
                    new KeyValuePair<string, ConsoleColor>("QUIT",ConsoleColor.Green),
                    new KeyValuePair<string, ConsoleColor>("\", \"",ConsoleColor.Gray),
                    new KeyValuePair<string, ConsoleColor>("USER",ConsoleColor.Green),
                    new KeyValuePair<string, ConsoleColor>("\", \"",ConsoleColor.Gray),
                    new KeyValuePair<string, ConsoleColor>("GEN",ConsoleColor.Green),
                    new KeyValuePair<string, ConsoleColor>("\", \"",ConsoleColor.Gray),
                    new KeyValuePair<string, ConsoleColor>("FAMILY",ConsoleColor.Green),
                    new KeyValuePair<string, ConsoleColor>("\" or \"",ConsoleColor.Gray),
                    new KeyValuePair<string, ConsoleColor>("RESULT",ConsoleColor.Green),
                    new KeyValuePair<string, ConsoleColor>("\"",ConsoleColor.Gray)
            });
        }

        public static void GenMessage(string left, string right)
        {
            ConsoleWriteLineWithColorAndClear("Test Generated:");
            ConsoleWriteLineWithColor(new KeyValuePair<string, ConsoleColor>[]
            {
                new KeyValuePair<string, ConsoleColor>(left,ConsoleColor.Yellow),
                new KeyValuePair<string, ConsoleColor>("/",ConsoleColor.Gray),
                new KeyValuePair<string, ConsoleColor>(right,ConsoleColor.Yellow)
            });
            ConsoleWriteLineWithColor(new KeyValuePair<string, ConsoleColor>[]
            {
                new KeyValuePair<string, ConsoleColor>("Please choose \"",ConsoleColor.Gray),
                new KeyValuePair<string, ConsoleColor>("LEFT",ConsoleColor.Green),
                new KeyValuePair<string, ConsoleColor>("\" or \"",ConsoleColor.Gray),
                new KeyValuePair<string, ConsoleColor>("RIGHT",ConsoleColor.Green),
                new KeyValuePair<string, ConsoleColor>("\" name",ConsoleColor.Gray)
            });
        }
        
        public static void InsertPrefferedGenderMessage()
        {
            ConsoleWriteLineWithColorAndClear(new KeyValuePair<string, ConsoleColor>[]
            {
                    new KeyValuePair<string, ConsoleColor>("Insert preffered gender(\"",ConsoleColor.Gray),
                    new KeyValuePair<string, ConsoleColor>("Male",ConsoleColor.Green),
                    new KeyValuePair<string, ConsoleColor>("\", \"",ConsoleColor.Gray),
                    new KeyValuePair<string, ConsoleColor>("Female",ConsoleColor.Green),
                    new KeyValuePair<string, ConsoleColor>("\")",ConsoleColor.Gray)
            });
        }

        public static void ErrorMessage(string value = "Improper value, please enter it once again")
        {
            ConsoleWriteLineWithColorAndClear(value, ConsoleColor.Red);
        }
    }
}
