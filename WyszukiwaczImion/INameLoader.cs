﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WyszukiwaczImion
{
    interface INameLoader
    {
        IEnumerable<IName> LoadNames();
    }
}
