﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Logic;

namespace WyszukiwaczImion
{
    class Program
    {
        static void Main(string[] args)
        {

            State currentState;
            string filePath = "State.txt";
            if (File.Exists(filePath))
            {
                try
                {
                    currentState = State.Deserialize(filePath);
                }
                catch(SerializationException)
                {
                    currentState = new State(new List<IUser>(), new List<IFamilyAccount>(), null, null);
                }
                UIMessages.ConsoleTitle.ActiveFamily = currentState.activeFamily?.FamilyId ?? "Unknown";
                UIMessages.ConsoleTitle.ActiveUser = currentState.activeUser?.userName ?? "Unknown";
            }
            else
                currentState = new State(new List<IUser>(), new List<IFamilyAccount>(), null, null);
            
            INameGenderLoader nameLoader = new NameWebLoader();
            List<IName> names = new List<IName>();
            IEnumerable<IName> temporaryNames = null;
            nameLoader.TryLoadGenderNames(out temporaryNames, Gender.Male);
            names.AddRange(temporaryNames);
            nameLoader.TryLoadGenderNames(out temporaryNames, Gender.Female);
            names.AddRange(temporaryNames);

            string input;
            do
            {
                UIMessages.InsertMessage();
                input = Console.ReadLine();
                switch (input)
                {
                    case "USER":

                        UIMessages.ConsoleWriteLineWithColorAndClear("Insert your username:");

                        input = Console.ReadLine();

                        if (currentState.activeUser == null || currentState.activeUser.userName != input)
                        {
                            currentState.activeUser = currentState.users.FirstOrDefault(p => p.userName == input);
                           
                            if (currentState.activeUser == null)
                            {
                                User userNew = new User(input, new List<IFamilyTests>());
                                currentState.users.Add(userNew);
                                currentState.activeUser = userNew;
                            }
                        }
                        UIMessages.ConsoleTitle.ActiveUser = currentState.activeUser.userName;
                        break;

                    case "GEN":
                        if (currentState.activeFamily == null || currentState.activeUser == null)
                        {
                            UIMessages.ErrorMessage("You cannot call this option without setting \"FAMILY\" or \"USER\"");
                            Thread.Sleep(5000);
                            break;
                        }

                        ITest test = currentState.activeUser.GenerateTest(currentState.activeFamily);
                        UIMessages.GenMessage(test.namePair.Item1.name, test.namePair.Item2.name);
                        do
                        {
                            input = Console.ReadLine();
                            switch(input)
                            {
                                case "LEFT":
                                    test.FinishTest(TestWinner.LeftWon);
                                    break;
                                case "RIGHT":
                                    test.FinishTest(TestWinner.RightWon);
                                    break;
                                default:
                                    UIMessages.ErrorMessage();
                                    break;
                            }
                        }
                        while (input != "RIGHT" && input != "LEFT");
                        break;
                    case "FAMILY":
                        UIMessages.ConsoleWriteLineWithColorAndClear("Insert your family id:");
                        input = Console.ReadLine();

                        IFamilyAccount family = null;
                        if (currentState.activeFamily == null || currentState.activeFamily.FamilyId != input)
                        {
                            family = currentState.familyAccounts.FirstOrDefault(p => p.FamilyId == input);
                        }
                        if (currentState.activeFamily == null)
                        {
                            UIMessages.InsertPrefferedGenderMessage();
                            string inputGender = Console.ReadLine();
                            if (!(inputGender == "Male" || inputGender == "Female"))
                            {
                                UIMessages.ErrorMessage("You cannot insert gender different than Male or Female");
                                Thread.Sleep(5000);
                                break;
                            }
                            BabyGender gender;
                            Enum.TryParse(inputGender, out gender);

                            IFamilyAccount familyNew = new FamilyAccount(input, names, new List<IUser>(), gender);
                            currentState.familyAccounts.Add(familyNew);
                            family = familyNew;
                            currentState.activeFamily = family;
                            UIMessages.ConsoleTitle.ActiveFamily = currentState.activeFamily.FamilyId;
                        }
                        currentState.activeFamily = family;
                        UIMessages.ConsoleTitle.ActiveFamily = currentState.activeFamily.FamilyId;
                        break;
                    case "RESULT":
                        if (currentState.activeFamily == null || currentState.activeUser == null)
                        {
                            UIMessages.ErrorMessage("You cannot call this option without setting \"FAMILY\" OR \"USER\"");
                            Thread.Sleep(5000);
                            break;
                        }
                        IResults testsResults = currentState.activeUser.GetResultsForFamily(currentState.activeFamily);
                        string resultString = testsResults.Normalize().ReturnAsOneString();
                        
                        //Merge into one string delimitted with newline chars
                        resultString += "\r\n Write anything to continue";
                        UIMessages.ConsoleWriteLineWithColorAndClear(resultString);
                        Console.ReadLine();
                        break;
                    case "QUIT":
                        currentState.Serialize(filePath);
                        break;
                    default:
                        UIMessages.ErrorMessage();
                        break;
                }
            }
            while (input != "QUIT");
        }
    }
}
