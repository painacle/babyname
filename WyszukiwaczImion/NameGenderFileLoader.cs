﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WyszukiwaczImion
{
    class NameGenderFileLoader: INameLoader
    {
        public string filePath { get; private set; } = "file.txt";
        public IEnumerable<IName> LoadNames()
        {
            List<IName> nameList = new List<IName>();
            using (StreamReader streamReader = new StreamReader(filePath))
            {
                string line;

                while (!streamReader.EndOfStream)
                {
                    line = streamReader.ReadLine();
                    int i = line.IndexOf(';');
                    string gender = line.Remove(0, i);
                    if (gender == ";FEMALE")
                        nameList.Add(new NameWithGender(line.Remove(i,gender.Length), NameWithGender.Gender.Female));
                    else if (gender == ";MALE")
                        nameList.Add(new NameWithGender(line.Remove(i,gender.Length), NameWithGender.Gender.Male));
                }       
            }
            return nameList;
        }
    }
}
